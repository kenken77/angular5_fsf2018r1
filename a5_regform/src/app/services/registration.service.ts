import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { RegistrationUser } from '../shared/registration-user';
import { Observable} from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

/**
 * The Constructor is a default method of the class that is executed when the class is 
 * instantiated and ensures proper initialization of fields in the class and its subclasses. 
 * Angular or better Dependency Injector (DI) analyzes the constructor parameters and
 *  when it creates a new instance by calling new MyClass() it tries to find providers 
 * that match the types of the constructor parameters, resolves them and passes them 
 * to the constructor like
 * 
 * ngOnInit is a life cycle hook called by Angular2 to indicate that Angular is done 
 * creating the component.We have to import OnInit in order to use like this (actually 
 * implementing OnInit is not mandatory but considered good practice):
 */

const httpOptions = { 
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class RegistrationService {
  
  private usersURL = `${environment.ApiUrl}/api/v1/users`;

  constructor(private httpClient:HttpClient, 
    private toastyService:ToastyService, 
    private toastyConfig: ToastyConfig) { }

  public saveRegisteredUser(user: RegistrationUser){
    return this.httpClient.post(this.usersURL, user, httpOptions)
    .pipe(catchError(this.handleError<RegistrationUser>('saveRegisteredUser')));
  };

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.addErrorToast("Error", JSON.stringify(error.error));
      return Observable.throw(error  || 'backend server error');
    };
  }

  addErrorToast(title, msg) {
    let toastOptions: ToastOptions = {
        title: title,
        msg: msg,
        showClose: true,
        timeout: 3500,
        theme: 'bootstrap',
        onAdd: (toast: ToastData) => {
            console.log('Toast ' + toast.id + ' has been added!');
        },
        onRemove: function(toast: ToastData) {
            console.log('Toast ' + toast.id + ' has been removed!');
        }
    };
    this.toastyService.error(toastOptions);
  }
}
