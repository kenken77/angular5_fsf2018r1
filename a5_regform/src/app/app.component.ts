
import { Component, OnInit } from '@angular/core';
import { RegistrationUser } from './shared/registration-user';
import { RegistrationService } from './services/registration.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  submitted = false;
  nationalities = [{desc: 'Singaporean', value: 'SG'}, {desc: 'Malaysian', value: 'MY'},
  {desc: 'British', value: 'UK'}, {desc: 'Australian', value: 'AU'}];

  model = new RegistrationUser('', '', '', '', 'M', null, '', '', '');

  constructor(
    private registrationService: RegistrationService,
    private toastyService:ToastyService, 
    private toastyConfig: ToastyConfig
  ){

  }

  onSubmit() {
    this.submitted = true;
    console.log(JSON.stringify(this.model));
    this.registrationService.saveRegisteredUser(this.model)
      .subscribe(user => {
        console.log(user);
        this.addSuccessToast('Successfully added', `Added ${this.model.fullName}`);
      });
  }

  onChange(evt) {
    //TODO
  }

  get diagnostic() { return JSON.stringify(this.model); }

  ngOnInit() {
  }

  addSuccessToast(title,msg) {
    var toastOptions:ToastOptions = {
        title: title,
        msg: msg,
        showClose: true,
        timeout: 1500,
        theme: 'bootstrap',
        onAdd: (toast:ToastData) => {
            console.log('Toast ' + toast.id + ' has been added!');
        },
        onRemove: function(toast:ToastData) {
            console.log('Toast ' + toast.id + ' has been removed!');
        }
    };
    this.toastyService.success(toastOptions);
  }

}
