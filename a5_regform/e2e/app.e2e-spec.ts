import { RegformPage } from './app.po';

describe('regform App', () => {
  let page: RegformPage;

  beforeEach(() => {
    page = new RegformPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
